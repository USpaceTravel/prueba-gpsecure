<?php

class Empleado
{

    private $id;
    private $nombre;
    private $email;
    private $area_id;
    private $boletin;
    private $descripcion;
    private $pdo;

    public function __construct()
    {
        try {

            $this->pdo = new Database;
        } catch (PDOException $e) {

            die($e->getMessage());
        }
    } # fin metodo constructor


    public function getAll()
    {
        try {
            $strSql = "SELECT * FROM empleado ";
            $query = $this->pdo->select($strSql);
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    } # fin metodo getAll

    public function newEmpleado($data)
    {
        try {
            if ($this->pdo->insert('empleado', $data)) {

                return true;
                
            } else {
                return false;
            }
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    } // fin metodo newEmpleado

    public function getById($id)
    {
        try {
            $strSql = 'SELECT * FROM empleado WHERE id = :id';
            $array = ['id' => $id];
            $query = $this->pdo->select($strSql, $array);
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function editEmpleado($data)
    {
        try {
            $srtWhere = 'id = ' . $data['id'];

            if ($this->pdo->update('empleado', $data, $srtWhere)) {

                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function deleteEmpleado($data)
    {
        try {
            $srtWhere = 'id = ' . $data['id'];

            if ($this->pdo->delete('empleado', $srtWhere)) {

                return true;

            } 
            else {
                return false;
            }

            
            
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }


}
