<?php

class Rol
{

    private $id;
    private $nombre;
    private $pdo;

    public function __construct()
    {
        try {

            $this->pdo = new Database;
        } catch (PDOException $e) {

            die($e->getMessage());
        }
    } # fin metodo constructor


    public function getAll()
    {
        try {
            $strSql = 'SELECT * FROM rol';
            $query = $this->pdo->select($strSql);
            return $query;
        } catch (PDOException $e) {

            die($e->getMessage());
        }
    } # fin metodo getAll

    public function newRol($data)
    {
        try {

            if ($this->pdo->insert('rol', $data)) {
                return true;
            } else {
                return false;
            }
            
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    } // fin metodo newUser

    public function getById($id)
    {
        try {
            $strSql = 'SELECT * FROM rol WHERE id = :id';
            $array = ['id' => $id];
            $query = $this->pdo->select($strSql, $array);
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function editRol($data)
    {
        try {
            $srtWhere = 'id = ' . $data['id'];
            $this->pdo->update('rol', $data, $srtWhere);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function deleteRol($data)
    {
        try {
            $srtWhere = 'id = ' . $data['id'];
            $this->pdo->delete('rol', $srtWhere);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
} # fin clase Rol
