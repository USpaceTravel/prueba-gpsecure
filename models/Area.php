<?php

class Area
{

    private $id;
    private $nombre;
    private $pdo;

    public function __construct()
    {
        try {

            $this->pdo = new Database;
        } catch (PDOException $e) {

            die($e->getMessage());
        }
    } # fin metodo constructor


    public function getAll()
    {
        try {
            $strSql = 'SELECT * FROM area';
            $query = $this->pdo->select($strSql);
            return $query;
        } catch (PDOException $e) {

            die($e->getMessage());
        }
    } # fin metodo getAll

    public function newArea($data)
    {
        try {

            if ($this->pdo->insert('area', $data)) {
                return true;
            } else {
                return false;
            }
            
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    } // fin metodo newUser

    public function getById($id)
    {
        try {
            $strSql = 'SELECT * FROM area WHERE id = :id';
            $array = ['id' => $id];
            $query = $this->pdo->select($strSql, $array);
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function editArea($data)
    {
        try {
            $srtWhere = 'id = ' . $data['id'];
            $this->pdo->update('area', $data, $srtWhere);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function deleteArea($data)
    {
        try {
            $srtWhere = 'id = ' . $data['id'];
            $this->pdo->delete('area', $srtWhere);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
} # fin clase area