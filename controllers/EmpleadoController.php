<?php

require 'models/Empleado.php';
require 'models/Rol.php';
require 'models/Area.php';

class EmpleadoController
{

    private $model;
    private $rol;
    private $areas;
    private $area;

    public function __construct()
    {
        $this->model = new Empleado;
        $this->rol = new Rol;
        $this->area = new Area;
    }

    public function index()
    {
        require 'views/home.php';
        $areas = $this->area->getAll();
        $personas = $this->model->getAll();
        require 'views/empleados/list.php';
        require 'views/footer.php';
        
    }

    public function add()
    {
        require 'views/home.php';
        $areas = $this->area->getAll();
        $roles = $this->rol->getAll();
        require('views/empleados/new.php');
        require 'views/footer.php';
    }

    public function save()
    {
        if(isset($_POST)){
            
            if ($this->model->newEmpleado($_REQUEST)) {
                echo json_encode(['success' => true]);
            } else {
                echo json_encode(['success' => false]);
            }
        }
        else{
            echo json_encode(['success' => 'vacio']);
        }
        
    }

    public function edit()
    {
        if (isset($_REQUEST['id'])) {
            $id = $_REQUEST['id'];
            $data = $this->model->getById($id);

            $roles = $this->role->getAll();

            require('views/layout.php');
            require 'views/footer.php';
        } else {
            echo "Error";
        }
    }

    public function update()
    {   
        if (isset($_POST)) {
            if ($this->model->editEmpleado($_POST)) {

                echo json_encode(['success' => true]);
            } else {
                echo json_encode(['success' => false]);
            }
        }

        else{
            echo json_encode(['success' => 'vacio']);
        }
    }

    public function delete()
    {
        if ($this->model->deleteempleado($_POST)) {

            echo json_encode(['success' => true]);
        } else {
            echo json_encode(['success' => false]);
        }
    }

    public function getidinfo(){

        $productos = $this->model->getById($_REQUEST['id']);
        $roles = $this->rol->getAll();
        $areas = $this->area->getAll();

        if($productos){

            echo json_encode(['mess' => 'Se consultó', 'result' => true, 'row'=> $productos, 'roles'=> $roles, 'areas'=> $areas]);

        }
        else{
            echo json_encode(['mess' => 'No se consultó', 'result' => false]);
        }
    }

}
