

function addEmpleado(){
    let nombre = $('#nombre').val();
    let correo = $('#correo').val();
    let sexo = $('input[name="form-check"]:checked').val();
    let area = $('#area_id').val();
    let descripcion = $('#descripcion').val();
    let boletin = $("#boletin").attr("checked") ? 1 : 0;
    let roles = $('input[name="form-check1"]:checked').val();

    let Datas = {
        'id': null,
        'nombre': nombre,
        'email': correo,
        'sexo': sexo,
        'area_id': area,
        'boletin': boletin,
        'descripcion': descripcion,
    }

    $.ajax({
        url:"?controller=empleado&method=save",
        type:"POST",
        data:Datas,
        success:function(response)
        {
            let {success} = JSON.parse(response);
            console.log(success);
            if(success){
                alert("Empleado registrado");
                window.location = '?controller=empleado&method=index';
            }
            else{
                alert("Error al registrar empleado");
            }
        }
      });

    
    
}




$(document).ready(function() {  	
	$('#formRegisterEmpleado').submit(function(e) {
    e.preventDefault();
	});
    
	$('#register').click(function(e) {
    e.preventDefault();
		addEmpleado();
	});

});