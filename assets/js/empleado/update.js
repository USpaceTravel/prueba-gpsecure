

function updateProducto(ids){
    
    let id = ids;
    let nombre = $('#nombre').val();
    let correo = $('#correo').val();
    let sexo = $('input[name="form-check"]:checked').val();
    let area = $('#area_id').val();
    let descripcion = $('#descripcion').val();
    let boletin = $("#boletin").attr("checked") ? 0 : 1;
    
    let Data = {
        'id': id,
        'nombre': nombre,
        'email': correo,
        'sexo': sexo,
        'area_id': area,
        'boletin': boletin,
        'descripcion': descripcion,
    }
    $.ajax({
        url:"?controller=empleado&method=update",
        type: "post",
        data: Data,
        })
        .done(function(response)
        {
            console.log(response);
            let {success} = JSON.parse(response);
            console.log(success);
            if(success){
            alert("Ejecución correcta");
            window.location = '?controller=empleado&method=index';
            }
            else{
                alert("Error al actualizar ");
            }
        });
                    


                     
        
  }


function listEmpleado(idr) {

    
    
    let id = idr;

    let Data = {
        'id': id,
    }
    
    $.ajax('?controller=empleado&method=getidinfo',{
      type: 'GET',
      dataType: 'json',
      data:Data,
        
    })
    .done(function(respuesta) {
      if (respuesta.result) {
        console.log(respuesta.row);
        let res = document.querySelector('#show');
        res.innerHTML = '';

        let table = respuesta.row;
        let roles = respuesta.roles;
        let areas = respuesta.areas;
        checket = '';
        let ss = '';
        let rol = '';
        let m = '';
        let f = '';

        for (let are of areas) {
            
      
            if(table[0]['area_id'] == are.id){
                ss += '<option value="'+are.id+'" selected>'+are.nombre+'</option>';
            }
            else{
                ss += '<option value="'+are.id+'">'+are.nombre+'</option>';
            }
           
        }

        for (let ro of roles) {
            
      
         
            rol += `<div class="form-check1 col-sm-9 text-left">
            <input class="form-check-input" type="radio" name="form-check1" id="${ro.id}" value="${ro.id}">
            <label class="form-check-label" for="f">
                 ${ro.nombre}
            </label>

        </div>

        <label for="" class="col-sm-3 col-form-label text-right"></label>`;
        

            
           
        }

        for (let item of table) {

            if(item.boletin == 1){
                checket = 'checked';
            }

            if(item.sexo == 'M'){
                m = 'checked';
            }
            else{
                f = 'checked';
            }
          res.innerHTML += `
          
          <div class="form-group row">
                            <label for="name" class="col-sm-3 col-form-label text-right">Nombre completo *</label>
                            <div class="col-sm-9">
                                <input type="text" id="nombre" class="form-control"
                                    placeholder="Nombre completo del empleado" value="${item.nombre}" required>
                            </div>

                        </div>


                        <div class="form-group row">
                            <label for="inputEmail" class="col-sm-3 col-form-label text-right">Correo electrónico
                                *</label>
                            <div class="col-sm-9">
                                <input type="email" value="${item.email}" id="correo" class="form-control" placeholder="Correo electrónico"
                                    required>
                            </div>

                        </div>

                        <div class="form-group row">
                            <label for="sexo" class="col-sm-3 col-form-label text-right">Sexo *</label>

                            <div class="form-check col-sm-9 text-left">
                                <input class="form-check-input" type="radio" name="form-check" id="masculino" value="M" ${m}>
                                <label class="form-check-label" for="m">
                                    Masculino
                                </label>

                            </div>

                            <label for="sexo" class="col-sm-3 col-form-label text-right"></label>

                            <div class="form-check col-sm-9 text-left">
                                <input class="form-check-input" type="radio" name="form-check" id="femenino" value="F" ${f}>
                                <label class="form-check-label" for="f">
                                    Femenino
                                </label>

                            </div>

                        </div>

                        <div class="form-group row">
                            <label for="area" class="col-sm-3 col-form-label text-right">Área *</label>
                            <div class="col-sm-9">

                                <select id="area_id" class="browser-default custom-select" list="area" value="${item.area_id}"
                                    placeholder="Seleccione" required>
                                    ${ss}
                                </select>
                            </div>

                        </div>



                        <div class="form-group row">
                            <label for="descripcion" class="col-sm-3 col-form-label text-right">Descripción *</label>
                            <div class="col-sm-9">
                                <textarea type="text" id="descripcion" class="form-control"
                                    placeholder="Descripción de la experiencia del empleado"
                                    required>${item.descripcion}</textarea>
                            </div>

                        </div>

                        <div class="form-group row">
                            <label for="" class="col-sm-3 col-form-label text-right"></label>


                            <div class="col-sm-9">
                                <input class="form-check-input col-sm-1 text-left" type="checkbox" id="boletin" ${checket}>
                                <label class="form-check-label col-sm-9" for="defaultCheck1">
                                    Deseo recibir boletín informativo
                                </label>

                            </div>

                        </div>
                        
                        <br>

                        <div class="form-group row">
                            <label for="" class="col-sm-3 col-form-label text-right">Roles *</label>

                            ${rol}
                            

                        </div>

                        <button id="ac" type="submit" onclick="updateProducto(${item.id});" class="btn btn-primary btn-block" value="${item.id}">Actualizar</a>

          `;
                      }

        
    }
      else {
      }
    })
  };


  function deleteProducto(ids){
    
    var answer = window.confirm("Desea eliminar?");
    if (answer) {
        
        let id = ids;
    
    
        let Data = {
            'id': id
        }
        $.ajax({
            url:"?controller=empleado&method=delete",
            type: "post",
            data: Data,
            })
            .done(function(response)
            {
                console.log(response);
                let {success} = JSON.parse(response);
                console.log(success);
                if(success){
                alert("Ejecución correcta");
                window.location = '?controller=empleado&method=index';
                }
                else{
                    alert("Error al eliminar ");
                }
            });
    }
    else {
        window.location = '?controller=empleado&method=index';
    }
    
                    


                     
        
  }


$(document).ready(function() {  	
    $('#formApplicantShow').submit(function(e) {
        e.preventDefault();
        });

});