<main class="container">
    <div class="modal fade" id="applicant1" tabindex="-1" role="form" aria-labelledby="modalApplicant1" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Información de la solicitud</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form method="POST" id="formApplicantShow">
                <fieldset>
                    <div id="show" class="modal-body">

                    </div>
                </fieldset>   
                </form>
            </div>
        </div>
    </div>
    <section class="col-md-12 text-center">
        <br>
        <br>
        <h1 class="text-left">Lista empleados</h1>

   
        <a id="nuevo" class="btn btn-primary float-right" href="?controller=empleado&method=add"> <i class="fa-solid fa-user-plus"></i> Crear</a>
        <br>
        <br>
        <section class="col-md-12 flex-nowrap">

            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th><i class="fa-solid fa-user"></i> Nombre</th>
                        <th><i class="fa-solid fa-at"></i> Email</th>
                        <th><i class="fa-solid fa-venus-mars"></i> Sexo</th>
                        <th><i class="fa-solid fa-briefcase"></i> Area</th>
                        <th><i class="fa-solid fa-envelope"></i> Boletin</th>
                        <th>Modificar</th>
                        <th>Eliminar</th>
                    </tr>
                </thead>

                <tbody>
                    <?php 
                    if (isset($personas)):
                        
                        foreach ($personas as $personass) : ?>
                            <tr>
                                <td><?php echo $personass->nombre ?> </td>
                                <td><?php echo $personass->email ?> </td>
                                <td> <?php  $sexo = ($personass->sexo == "M") ? "Masculino" : "Femenino"; echo $sexo; ?></td>
                                <td> <?php if(isset($areas)):
                                                foreach($areas as $areass): 
                                                
                                                            if($personass->area_id ==  $areass->id){

                                                                echo $areass->nombre;
                                                            }

                                                endforeach;
                                            else:  
                                                echo $personass->area_id;
                                            endif;  ?></td>
                                <td> <?php $boletin = ($personass->boletin == 0) ? "No" : "Si"; echo $boletin;?></td>
                                <td>

                                    <a id="edit" onclick="listEmpleado(<?php echo $personass->id ?>);" data-toggle="modal" data-target="#applicant1" id="showInfo" value="<?php echo $personass->id ?>"> <i class="fa-solid fa-pen-to-square"></i> </button>
                                    
                                </td>

                                <td>
                                    <a id="delete" onclick="deleteProducto(<?php echo $personass->id ?>)"> <i class="fa-solid fa-trash-can"></i></a>
                                </td>

                            </tr>
                            
                        <?php endforeach ?>
                        <?php endif ?>

                </tbody>
            </table>

        </section>
    </section>
</main>

