<body class="bg-dark">
    <div class="container">
        <div class="col-md-8 mx-auto">
            <div class="card card-register mx-auto mt-6">
                <div class="card-header text-left">
                    <h1>Crear empleado</h1>
                </div>
                <div class="card-body bg-light">
                    <form method="POST" id="formRegisterEmpleado">
                    <fieldset>
                        <div class="form-group row">
                            <label for="name" class="col-sm-3 col-form-label text-right">Nombre completo *</label>
                            <div class="col-sm-9">
                                <input type="text" id="nombre" class="form-control"
                                    placeholder="Nombre completo del empleado" required>
                            </div>

                        </div>


                        <div class="form-group row">
                            <label for="inputEmail" class="col-sm-3 col-form-label text-right">Correo electrónico
                                *</label>
                            <div class="col-sm-9">
                                <input type="email" id="correo" class="form-control" placeholder="Correo electrónico"
                                    required>
                            </div>

                        </div>

                        <div class="form-group row" required>
                            <label for="sexo" class="col-sm-3 col-form-label text-right">Sexo *</label>

                            <div class="form-check col-sm-9 text-left">
                                <input class="form-check-input" type="radio" name="form-check" id="masculino" value="M">
                                <label class="form-check-label" for="m">
                                    Masculino
                                </label>

                            </div>

                            <label for="sexo" class="col-sm-3 col-form-label text-right"></label>

                            <div class="form-check col-sm-9 text-left">
                                <input class="form-check-input" type="radio" name="form-check" id="femenino" value="F">
                                <label class="form-check-label" for="f">
                                    Femenino
                                </label>

                            </div>

                        </div>

                        <div class="form-group row">
                            <label for="area" class="col-sm-3 col-form-label text-right">Área *</label>
                            <div class="col-sm-9">

                                <select id="area_id" class="browser-default custom-select" list="area"
                                    placeholder="Seleccione" required>
                                    <?php
                                    foreach ($areas as $areass) : ?>
                                    <option value="<?php echo $areass->id ?>"><?php echo $areass->nombre ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>

                        </div>



                        <div class="form-group row">
                            <label for="descripcion" class="col-sm-3 col-form-label text-right">Descripción *</label>
                            <div class="col-sm-9">
                                <textarea type="text" id="descripcion" class="form-control"
                                    placeholder="Descripción de la experiencia del empleado"
                                    required></textarea>
                            </div>

                        </div>

                        <div class="form-group row">
                            <label for="" class="col-sm-3 col-form-label text-right"></label>


                            <div class="col-sm-9">
                                <input class="form-check-input col-sm-1 text-left" type="checkbox" value="1" id="boletin">
                                <label class="form-check-label col-sm-9" for="defaultCheck1">
                                    Deseo recibir boletín informativo
                                </label>

                            </div>

                        </div>
                        
                        <br>

                        <div class="form-group row">
                            <label for="" class="col-sm-3 col-form-label text-right">Roles *</label>

                            <?php foreach($roles as $roless):
                                ?>
                                

                                <div class="form-check1 col-sm-9 text-left">
                                    <input class="form-check-input" type="radio" name="form-check1" id="<?php echo $roless->id ?>" value="<?php echo $roless->id ?>">
                                    <label class="form-check-label" for="f">
                                        <?php echo $roless->nombre ?>
                                    </label>

                                </div>

                                <label for="" class="col-sm-3 col-form-label text-right"></label>

                                <?php endforeach; ?>
                            

                        </div>

                        <input id="register" type="submit" class="btn btn-primary btn-block" value="Registar"></input>
                        
                    </fieldset>
                    </form>
                    <div class="text-center">
                        <a class="d-block small mt-3" href="?controller=empleado&method=index">Cancelar</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>


